package presentation;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

@SuppressWarnings("serial")
public class Form extends JFrame{
	private JTextField field1;
	private JTextField field2;
	private JTextField field3;
	private JTextField field4;
	private JButton ok;
	public Form(String val1, String val2, String val3, String val4) {
		super("Form");
		this.field1 = new JTextField(val1, 3);
		this.field2 = new JTextField(val2, 10);
		this.field3 = new JTextField(val3, 20);
		this.field4 = new JTextField(val4, 20);
		this.ok = new JButton("OK");
		JPanel container = new JPanel();
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setSize(400, 400);
		this.setVisible(true);
		this.add(container);
		container.add(field1);
		container.add(field2);
		container.add(field3);
		container.add(field4);
		container.add(ok);
		this.pack();
	}
	
	public String getVal1() {
		return field1.getText();
	}

	public String getVal2() {
		return field2.getText();
	}

	public String getVal3() {
		return field3.getText();
	}

	public String getVal4() {
		return field4.getText();
	}

	public JButton getButton() {
		return this.ok;
	}
}
