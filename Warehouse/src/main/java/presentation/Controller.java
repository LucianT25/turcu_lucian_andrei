package presentation;

import java.awt.Dimension;
import java.lang.reflect.Field;
import java.util.List;
import javax.swing.JTable;

public class Controller {
	
	public Controller() {
	}
	
	public JTable createTable(List<?> itemList) {
		int k = 0;
		String[] columns = new String[4];
		Class<?> theClass = itemList.get(0).getClass();
		
		for(Field field : theClass.getDeclaredFields()) {
			field.setAccessible(true);
			columns[k] = field.getName();
			k++;
		}
		
		String[][] data = new String[itemList.size()+1][4];
		for(int i = 0; i < 4; i++) {
			data[0][i] = columns[i];
		}
		try {
			for(int i = 1; i < itemList.size()+1; i++) {
				int j = 0;
				for(Field field : theClass.getDeclaredFields()) {
					field.setAccessible(true);
					data[i][j] = String.valueOf(field.get(itemList.get(i-1)));
					j++;
				}
			}
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		
		JTable source = new JTable(data, columns);
		source.setPreferredScrollableViewportSize(new Dimension(450, 60));
		source.setFillsViewportHeight(true);
		return source;
	}
	
	
}
