package interfaces;

public interface Subject {
	public void notifyObserver(Double change);
}
