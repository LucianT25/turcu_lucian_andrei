package GUI;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.UUID;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.EmptyBorder;

import entities.Account;
import entities.Person;

@SuppressWarnings("serial")
public class AccountsWindow extends JFrame {

	private JPanel contentPane;
	private Person client;
	private JTable table;
	private JTextField textField;

	/**
	 * Create the frame.
	 */
	public AccountsWindow(Person client) {
		setTitle("Accounts");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 410, 353);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		this.client = client;
		JScrollPane scrollPane = new JScrollPane();
		
		JButton btnNew = new JButton("New");
		btnNew.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AddAccount aa = new AddAccount(client);
				aa.setVisible(true);
			}
		});
		
		JButton btnDelete = new JButton("Delete");
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(table.getSelectedRow() == -1) {
					JOptionPane.showMessageDialog(null, "Please select a row.", "Error", 0);
					return;
				}
				UUID id = UUID.fromString((String)table.getModel().getValueAt(table.getSelectedRow(), 0));
				Account account = MainMenu.getBank().searchAccountById(client, id);
				MainMenu.getBank().removeAccount(account);
				table = populateAccountsTable();
				scrollPane.setViewportView(table);
			}
		});
		
		JButton btnWithdraw = new JButton("Withdraw");
		btnWithdraw.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(table.getSelectedRow() == -1) {
					JOptionPane.showMessageDialog(null, "Please select a row.", "Error", 0);
					return;
				}
				UUID id = UUID.fromString((String)table.getModel().getValueAt(table.getSelectedRow(), 0));
				Account account = MainMenu.getBank().searchAccountById(client, id);
				
				if(textField.getText().equals("")) {
					JOptionPane.showMessageDialog(null, "Please enter a sum", "Error", 0);
					return;
				}
				
				account.withdraw(Double.valueOf(textField.getText()));
				table = populateAccountsTable();
				scrollPane.setViewportView(table);
			}
		});
		
		JButton btnDeposit = new JButton("Deposit");
		btnDeposit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(table.getSelectedRow() == -1) {
					JOptionPane.showMessageDialog(null, "Please select a row.", "Error", 0);
					return;
				}
				
				UUID id = UUID.fromString((String)table.getModel().getValueAt(table.getSelectedRow(), 0));
				Account account = MainMenu.getBank().searchAccountById(client, id);
				
				if(textField.getText().equals("")) {
					JOptionPane.showMessageDialog(null, "Please enter a sum", "Error", 0);
					return;
				}
				
				account.deposit(Double.valueOf(textField.getText()));
				table = populateAccountsTable();
				scrollPane.setViewportView(table);
			}
		});
		
		
		JButton btnRefresh = new JButton("Refresh");
		btnRefresh.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				table = populateAccountsTable();
				scrollPane.setViewportView(table);
			}
		});
		
		textField = new JTextField();
		textField.setColumns(10);
		
		JLabel lblSum = new JLabel("Sum:");
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 576, Short.MAX_VALUE)
				.addGroup(Alignment.TRAILING, gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(btnNew, GroupLayout.PREFERRED_SIZE, 73, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(btnDelete, GroupLayout.PREFERRED_SIZE, 73, GroupLayout.PREFERRED_SIZE))
						.addComponent(btnRefresh))
					.addPreferredGap(ComponentPlacement.RELATED, 42, Short.MAX_VALUE)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
						.addComponent(btnDeposit)
						.addComponent(lblSum, GroupLayout.PREFERRED_SIZE, 29, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING, false)
						.addComponent(textField, 0, 0, Short.MAX_VALUE)
						.addComponent(btnWithdraw, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
					.addContainerGap())
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(Alignment.TRAILING, gl_contentPane.createSequentialGroup()
					.addGap(38)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnNew)
						.addComponent(btnDelete)
						.addComponent(btnWithdraw)
						.addComponent(btnDeposit))
					.addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(textField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(btnRefresh)
						.addComponent(lblSum))
					.addGap(18)
					.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 200, GroupLayout.PREFERRED_SIZE))
		);
		
		table = new JTable();
		table = populateAccountsTable();
		scrollPane.setViewportView(table);
		contentPane.setLayout(gl_contentPane);
	}
	
	public JTable populateAccountsTable() {
		String[] columns = new String[] {"id", "owner", "balance", "type"};
		JTable table = new JTable(MainMenu.getBank().outputAccountsOfClient(this.client), columns);
		return table;
	}
}
