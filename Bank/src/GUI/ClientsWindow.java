package GUI;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

@SuppressWarnings("serial")
public class ClientsWindow extends JFrame {
	private JPanel contentPane;
	private JTable table;

	/**
	 * Create the frame.
	 */
	public ClientsWindow() {
		setTitle("Clients");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 465, 351);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		JScrollPane scrollPane = new JScrollPane();
		
		JButton btnNew = new JButton("New");
		btnNew.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				AddPerson dialog = new AddPerson();
				dialog.setVisible(true);
			}
		});
		

		JButton btnDelete = new JButton("Delete");
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(table.getSelectedRow() == -1) {
					JOptionPane.showMessageDialog(null, "Please select a row.", "Error", 0);
					return;
				}
				int id = Integer.valueOf((String)table.getModel().getValueAt(table.getSelectedRow(), 0));
				MainMenu.getBank().removePerson(MainMenu.getBank().searchClientById(id));
				table = populateClientsTable();
				scrollPane.setViewportView(table);
			}
		});
		
		//String[] columns = new String[] {"id", "name"};
		//table = new JTable(MainMenu.getBank().outputClients(), columns);
		table = new JTable();		
		table = populateClientsTable();
		scrollPane.setViewportView(table);
		
		JButton btnRefresh = new JButton("Refresh");
		btnRefresh.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				table = populateClientsTable();
				scrollPane.setViewportView(table);
			}
		});
		
		JButton btnManage = new JButton("Manage");
		btnManage.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(table.getSelectedRow() == -1) {
					JOptionPane.showMessageDialog(null, "Please select a row.", "Error", 0);
					return;
				}
				int id = Integer.valueOf((String)table.getModel().getValueAt(table.getSelectedRow(), 0));
				AccountsWindow aw = new AccountsWindow(MainMenu.getBank().searchClientById(id));
				aw.setVisible(true);
			}
		});
		
		JButton btnUpdate = new JButton("Update");
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(table.getSelectedRow() == -1) {
					JOptionPane.showMessageDialog(null, "Please select a row.", "Error", 0);
					return;
				}
				int id = Integer.valueOf((String)table.getModel().getValueAt(table.getSelectedRow(), 0));
				EditWindow ew = new EditWindow(MainMenu.getBank().searchClientById(id));
				ew.setVisible(true);
			}
		});
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 419, Short.MAX_VALUE)
							.addContainerGap())
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(btnNew)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(btnDelete)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(btnUpdate)
							.addPreferredGap(ComponentPlacement.RELATED, 50, Short.MAX_VALUE)
							.addComponent(btnManage)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(btnRefresh)
							.addGap(10))))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnNew)
						.addComponent(btnDelete)
						.addComponent(btnRefresh)
						.addComponent(btnManage)
						.addComponent(btnUpdate))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 251, GroupLayout.PREFERRED_SIZE)
					.addContainerGap())
		);
		contentPane.setLayout(gl_contentPane);
	}
	
	
	public JTable populateClientsTable() {
		String[] columns = new String[] {"id", "name", "spending bal.", "savings bal."};
		JTable table = new JTable(MainMenu.getBank().outputClients(), columns);
		return table;
	}
}
