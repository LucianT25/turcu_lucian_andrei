package GUI;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.LayoutStyle.ComponentPlacement;

import entities.Bank;

import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.awt.event.ActionEvent;

public class MainMenu {
	private int month = 0;
	private JFrame frmBank;
	private static Bank bank;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				File inputFile = new File("bank.ser");
				if(inputFile.exists()) {
					try {
					ObjectInputStream input = new ObjectInputStream(new FileInputStream("bank.ser"));
					MainMenu.bank = (Bank) input.readObject();
					input.close();
					} catch (FileNotFoundException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					} catch (ClassNotFoundException e) {
						e.printStackTrace();
					}				
				}
				else {
					MainMenu.bank = new Bank();
				}
				try {
					MainMenu window = new MainMenu();
					window.frmBank.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public MainMenu() {
		initialize();
	}

	private void initialize() {
		
		frmBank = new JFrame();
		frmBank.setTitle("Bank");
		frmBank.setBounds(100, 100, 230, 250);
		frmBank.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmBank.addWindowListener(new WindowAdapter()
		{
			public void windowClosing(WindowEvent windowEvent)
			{
				try {
					ObjectOutputStream output = new ObjectOutputStream(new FileOutputStream("bank.ser"));
					output.writeObject(bank);
					output.close();
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}								
			}
		});
		
		JButton clientsButton = new JButton("Clients");
		clientsButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ClientsWindow cw = new ClientsWindow();
				cw.setVisible(true);
			}
		});
		
		JButton reportButton = new JButton("Report");
		reportButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				bank.generateReport();
			}
		});
		
		JButton timeButton = new JButton("Advance Time ("+incMonth()+")");
		timeButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				timeButton.setText("Advance Time ("+incMonth()+")");
				bank.addInterest();
			}
		});
		
		JLabel lblMainMenu = new JLabel("Main Menu");
		lblMainMenu.setFont(new Font("Tahoma", Font.BOLD, 15));
		GroupLayout groupLayout = new GroupLayout(frmBank.getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(65)
							.addComponent(lblMainMenu))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(48)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
								.addComponent(timeButton, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(clientsButton, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(reportButton, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
					.addContainerGap(49, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(22)
					.addComponent(lblMainMenu)
					.addGap(18)
					.addComponent(clientsButton)
					.addPreferredGap(ComponentPlacement.RELATED, 35, Short.MAX_VALUE)
					.addComponent(timeButton)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(reportButton)
					.addGap(42))
		);
		frmBank.getContentPane().setLayout(groupLayout);
	}
	
	
	public static Bank getBank() {
		return bank;
	}

	public int incMonth() {
		if(month == 12)
			month = 1;
		else
			month++;
		
		return month;
	}
	

}
