package tests;

import static org.junit.Assert.*;

import org.junit.Test;

import entities.Bank;
import entities.Person;
import entities.SavingAccount;
import entities.SpendingAccount;

public class StandardFunctionalityTest {

	@Test
	public void test() {
		Bank testBank = new Bank();
		Person testPerson = new Person(1, "Test Client");
		testBank.addPerson(testPerson);
		if(testBank.getNrOfClients() == 0) {
			fail("Client not added");
		}
		
		String prevName = testPerson.getName();
		testBank.updatePerson(testPerson, "updatedPerson");
		
		if(testPerson.getName().equals(prevName)) {
			fail("Client not updated");
		}
		
		SavingAccount testSavingAcc = new SavingAccount(1, 0, testPerson);
		SpendingAccount testSpendingAcc = new SpendingAccount(1, 0, testPerson);
		
		testBank.addAccount(testSavingAcc, testPerson);
		testBank.addAccount(testSpendingAcc, testPerson);
		if(testBank.getNrOfManagedAccounts() == 0) {
			fail("Accounts not added");
		}
		
		testSavingAcc.deposit(10);
		if(testSavingAcc.getBalance() != 10) {
			fail("Deposit not successful");
		}
		double prevBalance = testSavingAcc.getBalance();
		testBank.addInterest();
		if(testSavingAcc.getBalance() != prevBalance+(prevBalance*5)/100) {
			fail("Interest not applied");
		}
		testSavingAcc.withdraw(0);
		if(testSavingAcc.getBalance() != 0) {
			fail("Withdrawal not successful");
		}
		
		testSpendingAcc.deposit(10);
		if(testSpendingAcc.getBalance() != 10) {
			fail("Deposit not successful");
		}
		testSavingAcc.withdraw(0);
		if(testSavingAcc.getBalance() != 0) {
			fail("Withdrawal not successful");
		}
		
		testBank.removeAccount(testSavingAcc);
		testBank.removeAccount(testSpendingAcc);
		
		if(testBank.getNrOfManagedAccounts() != 0) {
			fail("Accounts not removed");
		}
		
		testBank.removePerson(testPerson);
		if(testBank.getNrOfClients() != 0) {
			fail("Client not removed");
		}
	}

}
