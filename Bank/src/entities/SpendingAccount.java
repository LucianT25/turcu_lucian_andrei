package entities;
import interfaces.Observer;

public class SpendingAccount extends Account{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public SpendingAccount(int cId, double balance, Observer o) {
		super(cId, balance, o);
	}
	
	@Override
	public double withdraw(double sum) {
		this.balance -= sum;
		notifyObserver(new Double(-sum)); //subtract the sum from the total
		return balance;
	}
	@Override
	public double deposit(double sum) {
		this.balance += sum;
		notifyObserver(new Double(sum)); //subtract the sum from the total
		return balance;
	}

}
