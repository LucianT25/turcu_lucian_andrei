package entities;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import interfaces.BankProc;

public class Bank implements BankProc, Serializable{

	private static final long serialVersionUID = 1L;
	private Map<Person, Set<Account>> bankFile;
	private List<Person> clients;
	private int nrOfClients;
	private int nrOfManagedAccounts;
	
	public Bank() {
		this.bankFile = new HashMap<>();
		this.clients = new ArrayList<>();
		this.nrOfClients = 0;
		this.nrOfManagedAccounts = 0;
	}
	
	@Override
	public void addPerson(Person p) {
		assert isWellFormed();
		assert !bankFile.containsKey(p);
		assert !clients.contains(p);
		int prev = nrOfClients;
		
		clients.add(p);
		bankFile.put(p, new HashSet<Account>());
		this.nrOfClients++;
		
		assert nrOfClients == prev + 1;
		assert bankFile.containsKey(p);
	}
	
	@Override
	public void removePerson(Person p) {
		assert isWellFormed();
		assert nrOfClients != 0;
		assert bankFile.containsKey(p);
		assert clients.contains(p);
		int prev = nrOfClients;
		
		clients.remove(p);
		bankFile.remove(p);
		nrOfClients--;
		
		assert nrOfClients == prev - 1;
		assert !bankFile.containsKey(p);
		assert !clients.contains(p);
	}
	
	@Override
	public void updatePerson(Person p, String name) {
		assert isWellFormed();
		assert clients.contains(p);
		
		String prev = clients.get(clients.indexOf(p)).getName();
		clients.get(clients.indexOf(p)).setName(name);
		
		assert !clients.get(clients.indexOf(p)).getName().equals(prev);
	}
	
	@Override
	public void addAccount(Account a, Person p) {
		assert isWellFormed();
		assert clients.contains(p);
		assert !bankFile.get(p).contains(a);
		int prev = nrOfManagedAccounts;
		
		bankFile.get(p).add(a);
		nrOfManagedAccounts++;
		
		assert nrOfManagedAccounts == prev + 1;
		assert bankFile.get(p).contains(a);
	}
	
	@Override
	public void removeAccount(Account a) {
		assert isWellFormed();
		assert bankFile.get(searchClientById(a.getClientId())).contains(a);
		int prev = nrOfManagedAccounts;
		
		a.notifyObserver(-a.getBalance());
		bankFile.get((Person) a.getOwner()).remove(a);
		nrOfManagedAccounts--;
		
		assert nrOfManagedAccounts == prev - 1;
		assert !bankFile.get((Person) a.getOwner()).contains(a);
	}
	
	public String[][] outputClients() {
		String[][] data = new String[this.nrOfClients][4];
		int i = 0;
		for(Person client: clients) {
			data[i][0] = String.valueOf(client.getId());
			data[i][1] = client.getName();
			data[i][2] = String.valueOf(client.getSpendingBalance());
			data[i][3] = String.valueOf(client.getSavingBalance());
			
			i++;
		}
		return data;
	}
	
	public Person searchClientById(int id) {
		for(Person p: this.clients) {
			if(p.getId() == id)
				return p;
		}
		return null;
	}
	
	public Account searchAccountById(Person p, UUID id) {
		for(Account a: bankFile.get(p)) {
			if(a.getAccountId().equals(id))
				return a;
		}
		return null;
	}
	
	public String[][] outputAccountsOfClient(Person p) {
		String[][] data = new String[bankFile.get(p).size()][4];
		int i = 0;
		for(Account account: bankFile.get(p)) {
			data[i][0] = String.valueOf(account.getAccountId());
			Person owner = (Person) account.getOwner();
			data[i][1] = owner.getName();
			data[i][2] = String.valueOf(account.getBalance());
			if(account.getClass().equals(SavingAccount.class))
				data[i][3] = "Saving";
			else if(account.getClass().equals(SpendingAccount.class))
				data[i][3] = "Spending";
			
			i++;
		}
		return data;
	}
	
	public void addInterest() {
		for(Set<Account> accountSets: bankFile.values()) {
			for(Account account: accountSets) {
				if(account.getClass().equals(SavingAccount.class)) {
					account.addInterest();
				}
			}
			
		}
	}
	
	public void generateReport(){
		try {
			PrintWriter fw = new PrintWriter("Report.txt");
			for(Set<Account> accountSet: this.bankFile.values()) {
				for(Account account: accountSet) {
					fw.println(account.toString());
				}
			}
			fw.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
				
	}
	public boolean isWellFormed() {
		return (this.nrOfClients >= 0 && this.nrOfManagedAccounts >= 0);
	}

	public int getNrOfClients() {
		return nrOfClients;
	}

	public int getNrOfManagedAccounts() {
		return nrOfManagedAccounts;
	}
	
}
