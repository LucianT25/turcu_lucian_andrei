package entities;
import interfaces.Observer;

public class SavingAccount extends Account {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public SavingAccount(int cId, double balance, Observer o) {
		super(cId, balance, o);
	}
	
	@Override
	public double deposit(double sum) {
		this.balance += sum;
		notifyObserver(new Double(sum));
		return balance;
	}
	
	@Override
	public double withdraw(double sum) {
		double withdrawSum = balance;
		balance = 0;
		notifyObserver(new Double(-withdrawSum));
		return withdrawSum;
	}
	
	public void addInterest() {
		double interest = (5*balance) / 100;
		notifyObserver(new Double(interest)); //add interest to total
		balance += interest;
	}
	
}
