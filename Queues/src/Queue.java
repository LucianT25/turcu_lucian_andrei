/**
 * This class simulates a queue. It extends the Thread class 
 * because each of its instances must run on a separate thread.
 * 
 * 
 */
import java.io.PrintWriter;
import java.util.LinkedList;


public class Queue extends Thread {
	
	private boolean isOpen;
	private LinkedList<Customer> customers; //list containing customers
	private String queueName;
	private static int MAX_CUST = 20; //customer limit per queue
	private boolean isFull; 
	private Handler handler;
	private PrintWriter pw;
	
	public Queue(boolean open, String qn, Handler h, PrintWriter pw) {
		this.queueName = qn;
		this.isOpen = open;
		this.isFull = false;
		this.customers = new LinkedList<Customer>();
		this.handler = h;
		this.pw = pw;
	}
	
	
	public String getQueueName() {
		return queueName;
	}

	public boolean isOpen() {
		return isOpen;
	}

	public void setOpen(boolean open) {
		this.isOpen = open;
	}

	public boolean isFull() {
		return isFull;
	}
	
	public void setFull() {
		this.isFull = true;
	}
	
	public LinkedList<Customer> getCustomers() {
		return customers;
	}

	public void setCustomers(LinkedList<Customer> customers) {
		this.customers = customers;
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(this.queueName+":\n");
		for(Customer cust: this.customers) {
				sb.append("c"+cust.getID()+": "+cust.getTimeOfArrival().toString()+" ("+cust.getTimeInFront()+"/"+cust.getFinishTime()+")\n");
		}
		return sb.toString();
	}
	
	public void enqueue(Customer c) {
		if(isFull() != true) {
			logNewCustomer(c);
			this.customers.addLast(c);
			
			if(this.customers.size() == MAX_CUST) {
				this.isFull = true;
			}
		}
	}

	public void dequeue() {
		logServedCustomer(this.customers.getFirst());
		this.customers.removeFirst();
		if(this.customers.size() < MAX_CUST)
			this.isFull = false;
	}
	
	public void logServedCustomer(Customer c) {
		pw.append("["+this.handler.getCurrentTime()+"]: Customer "+c.getID()+" has been served\r\n");
	}
	
	public void logNewCustomer(Customer c) {
		pw.append("["+this.handler.getCurrentTime()+"]: Customer " + c.getID() + " has entered "+this.queueName+"\r\n");
}
	
	public void run() {
			try{		
				while(handler.getCurrentTime().compareTo(handler.getStopTime()) < 0) {
					sleep(1000);
					if(this.customers.isEmpty() != true) {	
						if(this.customers.getFirst().getTimeInFront() == this.customers.getFirst().getFinishTime()) {
								handler.incClientsServed();
								this.dequeue();
						} else {
								this.customers.getFirst().spendTimeInFront();
						}
					}	
				}
			} catch(InterruptedException e) {
				System.out.println("Thread interrupted!");
			}			
		}

}
