import java.time.LocalTime;

public class Customer {
	private int ID;
	private LocalTime timeOfArrival;
	private int finishTime;
	//private int waitingTime;
	private int timeInFront;
	public Customer(int nrID, LocalTime toa, int servTime) {
		this.ID = nrID;
		this.timeOfArrival = toa;
		this.finishTime = servTime;
		//this.waitingTime = 0;
		this.timeInFront = 0;
	}
	
	public LocalTime getTimeOfArrival() {
		return timeOfArrival;
	}

	public void setTimeOfArrival(LocalTime timeOfArrival) {
		this.timeOfArrival = timeOfArrival;
	}

	public int getFinishTime() {
		return finishTime;
	}

	public void setFinishTime(int finishTime) {
		this.finishTime = finishTime;
	}

	public int getID() {
		return ID;
	}

	/*public int getWaitingTime() {
		return waitingTime;
	}

	public void setWaitingTime(int waitingTime) {
		this.waitingTime = waitingTime;
	}*/
	
	public int getTimeInFront() {
		return timeInFront;
	}

	public void spendTimeInFront(){
		this.timeInFront += 1;
	}
	
}
