package jUnitTests;

import static org.junit.Assert.*;

import org.junit.Test;

import entities.polynomials.Polynomial;

public class DiffTest {

	@Test
	public void test() {
		Polynomial p1 = new Polynomial("25x^4+3.25x^3+9x^2-33x+66");	
		Polynomial result = p1.differentiate();		
		assertEquals("100x^3+9.75x^2+18x-33",result.toString());
	}

}
