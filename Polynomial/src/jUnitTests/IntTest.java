package jUnitTests;

import static org.junit.Assert.*;

import org.junit.Test;

import entities.polynomials.Polynomial;

public class IntTest {

	@Test
	public void test() {
		Polynomial p1 = new Polynomial("25x^4+3.25x^3+9x^2-33x+66");	
		Polynomial result = p1.integrate();		
		assertEquals("5x^5+0.812x^4+3x^3-16.5x^2+66x",result.toString());
	}

}
