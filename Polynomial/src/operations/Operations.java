package operations;

public interface Operations<T> {
public T addTo(T o);
public T subtract(T o);
public T multiplyBy(T o);
public T divideBy(T o);
public T integrate();
public T differentiate();

}
