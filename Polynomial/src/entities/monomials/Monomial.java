package entities.monomials;

import java.text.DecimalFormat;

import operations.Operations;

public class Monomial implements Operations<Monomial>{
private int power = 0;
private float coefficient = 0;

public Monomial(int p, float c) {
	this.power = p;
	this.coefficient = c;
}

public int getPower() {
	return this.power;
}

public float getCoeff() {
	return this.coefficient;
}

public void setCoeff(float c) {
	this.coefficient = c;
}


//Operations:
@Override
public Monomial addTo(Monomial o) {
	Monomial result = new Monomial(this.getPower(), this.getCoeff() + o.getCoeff());
	return result;
}

@Override
public Monomial subtract(Monomial o) {
	Monomial result = new Monomial(this.getPower(), this.getCoeff() - o.getCoeff());
	return result;
}

@Override
public Monomial multiplyBy(Monomial o) {
	Monomial result = new Monomial(this.getPower()+o.getPower(), ( this.getCoeff()*o.getCoeff() ));
	return result;
}

@Override
public Monomial divideBy(Monomial o) {
	Monomial result = new Monomial(this.getPower()-o.getPower(), ( this.getCoeff()/o.getCoeff() ));
	return result;
}

@Override
public Monomial integrate() {
	Monomial result = new Monomial(this.getPower()+1, this.getCoeff()/(this.getPower()+1));
	return result;
}

@Override
public Monomial differentiate() {
	Monomial result = null;
	if(this.getPower() == 0) {
		result = new Monomial(0, 0);
	}
	else {
		result = new Monomial(this.getPower()-1, this.getCoeff()*(this.getPower()));
	}
	return result;
}

public String toString() {
	DecimalFormat f = new DecimalFormat();
    f.setDecimalSeparatorAlwaysShown(false);
    
    if(this.coefficient != 0) {
    	if(this.power == 0) {
    		return String.format("%s", f.format(this.coefficient));
    	}
    	else if(this.power == 1) {
    		if(this.coefficient == 1){
    			return String.format("x");
    		}
    		else{
    			return String.format("%sx", f.format(this.coefficient));
    		}
    	}
    	else {
    		if(this.coefficient == 1){
    			return String.format("x^%d", this.power);
    		}
    		else {
    			return String.format("%sx^%d", f.format(this.coefficient), this.power);
    		}
    	}
    }
    else {
    	return "0";
    }
}
}
