package comparators;

import java.util.Comparator;

import entities.monomials.Monomial;

public class PowerComparator implements Comparator<Monomial> {
    public int compare(Monomial m1, Monomial m2) {
        return Integer.compare(m1.getPower(), m2.getPower());     
    }
}
