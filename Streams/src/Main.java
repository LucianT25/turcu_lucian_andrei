import java.awt.Desktop;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;


public class Main {
	
	public static void main(String[] args) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		List<MonitoredData> monitoredData = new ArrayList<>();
		List<String> readData = new LinkedList<>();
		BufferedReader fileReader = null;

		try {
			fileReader = Files.newBufferedReader(Paths.get("Activities.txt"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		readData = fileReader.lines().collect(Collectors.toList());
		
		readData.stream().forEach(data -> {
			monitoredData.add(
					new MonitoredData(LocalDateTime.parse(data.split("\\t\\\t")[0], formatter), LocalDateTime.parse(data.split("\\t\\t")[1], formatter), data.split("\\t\\t")[2]));
		});
		
		monitoredData.stream().forEach(e -> { //corrections for the extra tabs/spaces
			e.setActivityLabel(e.getActivityLabel().replaceAll("\\t", ""));
			e.setActivityLabel(e.getActivityLabel().replaceAll("\\s", ""));
		});
		
		/*for(MonitoredData entry: monitoredData) {
			System.out.println(entry.toString());
		}*/
		
		JFrame appFrame = new JFrame("Streams app");
		appFrame.setVisible(true);
		
		JPanel container = new JPanel();
		appFrame.add(container);
		appFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		container.setLayout(new FlowLayout());
		container.add(new JLabel("Task 1 result: "+task1(monitoredData)));

		JButton task2 = new JButton("Task 2");
		task2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				task2(monitoredData);
				try {
					Desktop.getDesktop().open(new java.io.File("task2.txt"));
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		});
		container.add(task2);
		
		JButton task3 = new JButton("Task 3");
		task3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				task3(monitoredData);
				try {
					Desktop.getDesktop().open(new java.io.File("task3.txt"));
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		});
		container.add(task3);
		
		JButton task4 = new JButton("Task 4");
		task4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				task4(monitoredData);
				try {
					Desktop.getDesktop().open(new java.io.File("task4.txt"));
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		});
		container.add(task4);
		
		JButton task5 = new JButton("Task 5");
		task5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				task5(monitoredData);
				try {
					Desktop.getDesktop().open(new java.io.File("task5.txt"));
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		});
		
		container.add(task5);
		appFrame.pack();
	}
	public static long task1(List<MonitoredData> monitoredData) {
		long dates = monitoredData.stream().map(entry -> entry.getStartTime().getDayOfMonth()).distinct().count(); //1
		System.out.println(dates);
		System.out.println();
		return dates;
	}
	
	public static void task2(List<MonitoredData> monitoredData) {
		Map<String, Long> activitiesMap = monitoredData.stream().collect(
				Collectors.groupingBy(MonitoredData::getActivityLabel, Collectors.counting())); //2
		
	
		for(Map.Entry<String, Long> me: activitiesMap.entrySet()) {
			System.out.println(me);
		}

		try {
			PrintWriter pw = new PrintWriter("task2.txt");
			for(Map.Entry<String, Long> me: activitiesMap.entrySet()) {
				pw.println("Day " + me.getKey() + ": " + me.getValue());
			}
			pw.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public static void task3(List<MonitoredData> monitoredData) {
		Map<Integer, Map<String, Long>> activitiesPerDay = monitoredData.stream().collect(
				Collectors.groupingBy(MonitoredData::getDayOfMonth, Collectors.groupingBy(MonitoredData::getActivityLabel, Collectors.counting())));
		
		try {
			PrintWriter pw = new PrintWriter("task3.txt");
			for(Map.Entry<Integer, Map<String, Long>> me: activitiesPerDay.entrySet()) {
				pw.println("Day " + me.getKey() + ": " + me.getValue());
			}
			pw.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public static void task4(List<MonitoredData> monitoredData) {
		Map<String, Long> aux = monitoredData.stream().collect(
				Collectors.groupingBy(MonitoredData::getActivityLabel, Collectors.summingLong(MonitoredData::getDurationInMillis)));		
		Map<String, Duration> durations = aux.entrySet().stream().filter(e -> e.getValue() < 36000000).collect(Collectors.toMap(e -> e.getKey(), e -> Duration.ofMillis(e.getValue())));
		
		try {
			PrintWriter pw = new PrintWriter("task4.txt");
			for(Map.Entry<String, Duration> me: durations.entrySet()) {
				pw.println(me.getKey() + ": " + me.getValue());
			}
			pw.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public static void task5(List<MonitoredData> monitoredData) {
		Map<String, Long> activitiesMap = monitoredData.stream().collect(
				Collectors.groupingBy(MonitoredData::getActivityLabel, Collectors.counting()));
		
		Map<String, Long> filteredCount = monitoredData.stream()
				.filter(e -> e.getDurationInMillis() < 300000)
				.collect(Collectors.groupingBy(MonitoredData::getActivityLabel, Collectors.counting())); //occurrences of durations shorter than 5 minutes;

		List<String> freq = activitiesMap.entrySet().stream()
				.filter(e -> filteredCount.get(e.getKey()) != null) //make sure the key exists in filteredCount
				.filter(e -> {
					System.out.println(e.getKey()+": "+(float)filteredCount.get(e.getKey())/(float)e.getValue()); //print for testing purposes
					return ((float)filteredCount.get(e.getKey())/(float)e.getValue()) < 0.90;
				}) //filter by computing the ratio of occurrences shorter than 5 minutes over total occurrences found in task 1
				.map(e -> e.getKey()).collect(Collectors.toList());
		
		activitiesMap.keySet().stream().filter(e -> !filteredCount.keySet().contains(e)).forEach(e -> freq.add(e)); //add the activities that always take longer
		
		try {
			PrintWriter pw = new PrintWriter("task5.txt");
			for(String activity: freq) {
				pw.println(activity);
			}
			pw.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		System.out.println();
	}
}
