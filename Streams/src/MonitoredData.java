import java.time.Duration;
import java.time.LocalDateTime;

public class MonitoredData {
	public LocalDateTime startTime;
	public LocalDateTime endTime;
	public String activityLabel;
	
	public MonitoredData(LocalDateTime startTime, LocalDateTime endTime, String activityLabel) {
		super();
		this.startTime = startTime;
		this.endTime = endTime;
		this.activityLabel = activityLabel;
	}

	public LocalDateTime getStartTime() {
		return startTime;
	}

	public void setStartTime(LocalDateTime startTime) {
		this.startTime = startTime;
	}

	public LocalDateTime getEndTime() {
		return endTime;
	}

	public void setEndTime(LocalDateTime endTime) {
		this.endTime = endTime;
	}

	public String getActivityLabel() {
		return activityLabel;
	}

	public void setActivityLabel(String activityLabel) {
		this.activityLabel = activityLabel;
	}
	
	public int getDayOfMonth(){
		return startTime.getDayOfMonth();
	}
	
	public Long getDurationInMillis() {
		/*int hours;
		int minutes;
		int seconds;
		hours = endTime.getHour() > startTime.getHour()? endTime.getHour() - startTime.getHour() : 24 - (startTime.getHour() + endTime.getHour());
		minutes = endTime.getMinute() > startTime.getMinute() ? endTime.getMinute() - startTime.getMinute() : 60 - (startTime.getMinute() - endTime.getMinute());
		seconds = endTime.getSecond() > startTime.getSecond() ? endTime.getSecond() - startTime.getSecond() : 60 - (startTime.getSecond() - endTime.getSecond());
		return LocalTime.of(hours, minutes, seconds);*/
		return Duration.between(startTime, endTime).toMillis();
	}
	
	@Override
	public String toString() {
		return "MonitoredData [startTime=" + startTime + ", endTime=" + endTime + ", activityLabel=" + activityLabel
				+ "]";
	}
	
	
}
